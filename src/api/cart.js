import request from '@/utils/request'

/**
 * 获取新的商品信息
 * @param {String} skuId 商品 skuId
 * @returns
 */
export const getNewCartGoods = skuId => {
  return request(`/goods/stock/${skuId}`, 'get')
}

/**
 * 合并本地购物车
 * @param {Array} cartList 本地购物车数组
 * @param {String} item.skuId 商品SkuId
 * @param {Boolean} item.selected 是否选中
 * @param {Integer} item.count 数量
 * @returns
 */
export const mergeLocalCart = cartList => {
  return request('/member/cart/merge', 'post', cartList)
}

/**
 * 获取登录后的购物车列表
 * @returns Promise
 */
export const findCartList = () => {
  return request('/member/cart', 'get')
}

/**
 * 加入购物车
 * @param {String} skuId 商品skuId
 * @param {Integer} count 商品数量
 * @returns
 */
export const insertCart = ({ skuId, count }) => {
  return request('/member/cart', 'post', { skuId, count })
}

/**
 * 删除商品
 * @param {Array} ids
 * @returns
 */
export const deleteCart = ids => {
  return request('/member/cart', 'delete', { ids })
}

/**
 * 修改购物车商品的状态和数量
 * @param {String} goods
 * @returns
 */
export const updateCart = goods => {
  return request('/member/cart/' + goods.skuId, 'put', goods)
}

/**
 * 全选反选
 * @param {Boolean} selected
 * @param {Array} ids
 * @returns
 */
export const checkAllCart = ({ selected, ids }) => {
  return request('/member/cart/selected', 'put', { selected, ids })
}
