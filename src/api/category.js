// 定义分类相关的API接口
import request from '@/utils/request'

/**
 * 获取首页头部分类数据
 */
export const findAllCategory = () => {
  return request('/home/category/head', 'get')
}

/**
 * 获取单个顶级分类信息
 * @param {String} id 顶级分类的id
 * @returns
 */
export const findTopCategory = id => {
  return request('/category', 'get', { id })
}

/**
 * 获取二级类目的筛选条件数据
 * @param {String} id 二级类目
 * @returns
 */
export const findSubCategoryFilter = id => {
  return request('/category/sub/filter', 'get', { id })
}

/**
 * 获取分类下的商品
 * @param {Object} params
 * @returns
 */
export const findSubCategoryGoods = params => {
  return request('/category/goods/temporary', 'post', params)
}
