import request from '@/utils/request'

/**
 * 获取商品详情
 * @param {String} id
 * @returns
 */
export const findGoods = id => {
  return request('/goods', 'get', { id })
}

/**
 * 获取商品同类推荐
 * @param {String} id
 * @param {Number} limit
 * @returns
 */
export const findRelGoods = (id, limit = 16) => {
  return request('/goods/relevant', 'get', { id, limit })
}

/**
 * 获取热榜商品
 * @param {Number} type - 1代表24小时热销榜 2代表周热销榜 3代表总热销榜
 * @param {Number} limit - 获取个数
 */
export const findHotGoods = ({ id, type, limit = 3 }) => {
  return request('/goods/hot', 'get', { id, type, limit })
}

/**
 * 查询商品评价信息
 * @param {String} id
 * @returns
 */
export const findGoodsCommentInfo = id => {
  return request(`https://mock.boxuegu.com/mock/1175/goods/${id}/evaluate`, 'get')
}

/**
 * 查询商品评价列表
 * @param {String} id
 * @param {Object} params
 * @returns
 */
export const findGoodsCommentList = (id, params) => {
  return request(`https://mock.boxuegu.com/mock/1175/goods/${id}/evaluate/page`, 'get', params)
}

/**
 * 获取商品的specs和skus
 * @param {String} skuId
 * @returns
 */
export const getSpecsAndSkus = skuId => {
  return request(`/goods/sku/${skuId}`, 'get')
}
