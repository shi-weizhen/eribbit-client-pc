import { createStore } from 'vuex'
import createPersistedstate from 'vuex-persistedstate'

import user from './modules/user'
import category from './modules/category'
import cart from './modules/cart'
// 创建vuex仓库并导出
export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    user,
    cart,
    category
  },
  plugins: [
    createPersistedstate({
      key: 'eribbit-client-pc',
      paths: ['user', 'cart']
    })
  ]
})
