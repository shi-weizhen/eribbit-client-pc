import defaultImg from '@/assets/images/200.png'

// import XtxSkeleton from './xtx-skeleton.vue'
// import XtxCarousel from './xtx-carousel'
// import XtxMore from './xtx-more'
// import XtxBread from './xtx-bread'
// import XtxBreadItem from './xtx-bread-item'

// 批量导入需要使用一个函数 require.context(dir,deep,matching)
// 参数 1.目录 2.是否加载子目录 3. 加载的正则匹配
const importFn = require.context('./', false, /\.vue$/)
export default {
  install (app) {
    // 在app上进行扩展，app提供 component directive 函数
    // 如果要挂载原型 app.config.globalProperties 方式
    // app.component(XtxSkeleton.name, XtxSkeleton)
    // app.component(XtxCarousel.name, XtxCarousel)
    // app.component(XtxMore.name, XtxMore)
    // app.component(XtxBread.name, XtxBread)
    // app.component(XtxBreadItem.name, XtxBreadItem)

    // 批量注册全局组件
    importFn.keys().forEach(key => {
      // 导入组件
      const component = importFn(key).default
      // 注册组件
      app.component(component.name, component)
    })

    // 定义指令
    defineDirective(app)
  }
}

// 定义指令
const defineDirective = (app) => {
  // 图片懒加载指令 v-lazyload
  // 原理: 先存储图片地址不能在src上,当图片进去可视区,将存储图片地址设置给图片元素即可
  app.directive('lazyload', {
    // vue2.0 监听使用指令的DOM是否创建好 钩子函数: inserted
    // vue3.0 的指令拥有的钩子函数和组件的一样,使用指令的DOM是否创建好,钩子函数: mounted
    mounted (el, binding) {
      // 创建一个观察对象 来观察当前使用指令的元素
      const observe = new IntersectionObserver(([{ isInterseting }]) => {
        if (isInterseting) {
          // 停止观察
          observe.unobserve(el)
          // 把指令的值设置给el的src属性 binding.value就是指令的值
          // 处理图片加载失败 error 图片加载失败的事件 load 图片加载成功
          el.onerror = () => {
            // 加载失败 设置默认图
            el.src = defaultImg
          }
          el.src = binding.value
        }
      }, {
        threshold: 0.01
      })
      // 开启观察
      observe.observe(el)
    }
  })
}
