// 创建一个新的axios实例
// 请求拦截器,携带token
// 响应拦截器:剥离无效数据 处理token失效
// 导出一个函数

import axios from 'axios'
import store from '@/store'
import router from '@/router'

// 有些地方不是通过axios发请求
export const baseURL = 'http://pcapi-xiaotuxian-front-devtest.itheima.net/'
const instance = axios.create({
  // axios的一些配置
  baseURL,
  timeout: 5000
})

instance.interceptors.request.use(config => {
  // 拦截业务逻辑
  // 进行请求配置的修改
  // 如果本地有token就在头部携带
  // 获取用户信息对象
  const { profile } = store.state.user
  // 判断是否有token
  if (profile.token) {
    // 设置token
    config.headers.Authorization = `Bearer ${profile.token}`
  }
  return config
}, err => {
  return Promise.reject(err)
})

// 响应拦截器 (res => res.data 取出 data 数据
instance.interceptors.response.use(res => res.data, err => {
  // 401状态码 进去该函数
  if (err.response && err.response.status === 401) {
    // 清空无效用户信息
    // 跳转到登录页
    // 跳转到当前路由地址 给登录页
    store.commit('user/setUser', {})
    // 当前路由地址
    const fullPath = encodeURIComponent(router.currentRoute.value.fullPath)
    // encodeURIComponent 转换为 uri 编码 防止解析地址出问题
    router.push('/login?redirectUrl=' + fullPath)
  }
  return Promise.reject(err)
})

// 请求工具函数
export default (url, method, submitData) => {
  // 负责发请求: 请求地址 请求方式 提交的数据
  return instance({
    url,
    method,
    // 如果是get请求 需要使用params来传递
    // 如果不是get请求 需要使用 data 来传递
    // []设置一个动态的key,写js表达式
    [method.toLowerCase() === 'get' ? 'params' : 'data']: submitData
  })
}
